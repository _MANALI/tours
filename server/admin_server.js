const config = require('config')
const express = require('express'); 
const allAdminRoutes = require('./routes/admin')
const showUser = require('./routes/user')
const allPackageRoutes = require('./routes/packages')

const app = express(); 

app.use((req, res, next)=>{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
})

app.use(express.json())
app.use("/admin",allAdminRoutes);
app.use("/user",showUser);
app.use("/package",allPackageRoutes);


app.get("/", (req, res)=>{
    res.send(`<h1> Welcome To Node</h1>`);
});

let portNo =  config.get("port")
app.listen(portNo, ()=>{ console.log("Server started listening...")});