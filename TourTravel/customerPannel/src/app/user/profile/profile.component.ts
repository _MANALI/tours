import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
users = []
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.loadUser()
  }
  loadUser() {
    this.userService
      .getUser()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.users = response['data']
          for (let index = 0; index < this.users.length; index++) {
            const user = this.users[index];
          }
        }
      })
  }
  onLoad()
  {
    this.loadUser()
  }
}
