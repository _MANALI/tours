import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddressAddComponent } from '../user/address-add/address-add.component';
import { AddressListComponent } from '../user/address-list/address-list.component';
import { BookingComponent } from './booking/booking.component';

const routes: Routes = [
  { path: 'mybooking', component: BookingComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MybookingRoutingModule { }
