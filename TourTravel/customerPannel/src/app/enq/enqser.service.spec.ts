import { TestBed } from '@angular/core/testing';

import { EnqserService } from './enqser.service';

describe('EnqserService', () => {
  let service: EnqserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnqserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
