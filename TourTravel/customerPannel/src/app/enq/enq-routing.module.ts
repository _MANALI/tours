import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EnquiryComponent } from './enquiry/enquiry.component';
import { IssueComponent } from './issue/issue.component';

const routes: Routes = [
  { path: 'enquiry', component: EnquiryComponent },
  { path: 'issue', component: IssueComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnqRoutingModule { }
