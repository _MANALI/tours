import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './auth/auth.service';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { 
    path: 'home',
    component: HomeComponent, 
    canActivate: [AuthService],
    children: [
      { path: 'user', loadChildren: () => import('./user/user.module').then(m => m.UserModule ) },
      { path: 'booking', loadChildren: () => import('./mybooking/mybooking.module').then(m => m.MybookingModule ) },
      { path: 'package', loadChildren: () => import('./package/package.module').then(m => m.PackageModule ) },
      { path: 'enq', loadChildren: () => import('./enq/enq.module').then(m => m.EnqModule ) },
      { path: 'about', loadChildren: () => import('./about/about.module').then(m => m.AboutModule ) }
    ]
  },
  { path: 'auth', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
