const config = require('config')
const mysql = require('mysql');     
const express = require('express'); 
const userRouteHandler =  express(); 

const connection = mysql.createConnection({
    host: config.get("host"),
    user: config.get("user"),
    password: config.get("password"),
    database: config.get("database")
});

var userData = null;
connection.connect(); 

userRouteHandler.get("/",(req,res) => {
    connection.query(`select u.UserId,firstName,lastName,phone,email,password,a.AddressId,a.address,a.city,a.country,a.zip from user u inner join address a on u.UserId = a.userId`,(err,result)=>{
        if(err == null  )
        {
            console.log(result)
            userData = result;
            res.contentType = "application/json";
            res.send(JSON.stringify(userData));
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
       });
    });
    userRouteHandler.get("/:id", (req, res)=>{
       connection.query(`select u.UserId,firstName,lastName,phone,email,password,a.AddressId,a.address,a.city,a.country,a.zip from user u inner join address a on u.UserId = a.userId where u.UserId=${req.params.id}`,(err,result)=>{
        if(err == null  )
        {
            userData = result;
            res.contentType = "application/json";
            res.send(JSON.stringify(userData));
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
    });
    userRouteHandler.delete("/:id",(req,res)=>{
        let no = req.params.id;

    let query = `delete from user where UserId=${no}`;
    console.log(query);

    connection.query(query,(err,result)=>{
        if(err == null  )
        {
            res.contentType = "application/json";
            res.send(JSON.stringify(result)); 
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
    });

    userRouteHandler.post("/", (req, res)=>{
        let first =  req.body.firstName;
        let last =  req.body.lastName;
        let phone = req.body.phone;
        let email = req.body.email;
        let password = req.body.password;
        let address = req.body.address;
        let city = req.body.city;
        let country = req.body.country;
        let zip = req.body.zip;

        let query = `insert into user (firstName,lastName,phone,email,password) values('${first}', '${last}','${phone}','${email}','${password}')`;
        console.log(query);
    
        let query1 = `Insert into address(address,city,country,zip,UserID) values('${address}','${city}','${country}','${zip}',(select UserID from user where email='${email}'))`;
        connection.query(query,query1,(err,result)=>{
            if(err == null  )
            {
                res.contentType = "application/json";
                res.send(JSON.stringify(result)); 
            }
            else
            {
                console.log("Error Occured. Details are " );
                console.log(err);
                res.send("Something went Wrong!")
            }
        });

        connection.query(query1,(err,result)=>{
            if(err == null  )
            {
                //res.contentType = "application/json";
                //res.send(JSON.stringify(result)); 
            }
            else
            {
                console.log("Error Occured. Details are " );
                console.log(err);
                //res.send("Something went Wrong!")
            }});
    });

    userRouteHandler.put("/:id", (req, res)=>{
        let id = req.params.id
        let first =  req.body.firstName;
        let last =  req.body.lastName;
        let phone = req.body.phone;
        let email = req.body.email;
        let password = req.body.password;
        let address = req.body.address;
        let city = req.body.city;
        let country = req.body.country;
        let zip = req.body.zip;

        let query = `update user set firstName='${first}',lastName='${last}',phone='${phone}',email='${email}',password='${password}' where UserId = ${id}`;
        console.log(query);
    
        let query1 = `update address set address='${address}',city = '${city}',country = '${country}',zip = '${zip}' where userID = ${id}`;
        connection.query(query,query1,(err,result)=>{
            if(err == null  )
            {
                res.contentType = "application/json";
                res.send(JSON.stringify(result)); 
            }
            else
            {
                console.log("Error Occured. Details are " );
                console.log(err);
                res.send("Something went Wrong!")
            }
        });

        connection.query(query1,(err,result)=>{
            if(err == null  )
            {
                //res.contentType = "application/json";
                //res.send(JSON.stringify(result)); 
            }
            else
            {
                console.log("Error Occured. Details are " );
                console.log(err);
                //res.send("Something went Wrong!")
            }});
    });
    module.exports = userRouteHandler;