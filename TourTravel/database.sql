DROP DATABASE Tourism;
create database Tourism;

USE Tourism

create table user (
	UserId integer PRIMARY KEY auto_increment, 
	firstName VARCHAR(100),
	lastName VARCHAR(100), 
        AddressId integer,
	phone VARCHAR(20),
	email VARCHAR(100),
	password VARCHAR(100),
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	activationToken VARCHAR(100)
);

create table address (
AddressId integer PRIMARY KEY auto_increment,  
address VARCHAR(100),
city VARCHAR(100),
country VARCHAR(100),
zip VARCHAR(100)
);

create table admin (
	AdminId integer PRIMARY KEY auto_increment, 
	firstName VARCHAR(100),
	lastName VARCHAR(100), 
	phone VARCHAR(20),
	email VARCHAR(100),
	password VARCHAR(100),
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	active INTEGER DEFAULT 1
);

CREATE TABLE package (
   PackageId integer PRIMARY KEY auto_increment,
   PackageName varchar(200) DEFAULT NULL,
   PackageType varchar(150) DEFAULT NULL,
   PackageLocation varchar(100) DEFAULT NULL,
   categoryId integer, 
   PackagePrice int(11) DEFAULT NULL,
   PackageFetures varchar(255) DEFAULT NULL,
   PackageDetails mediumtext DEFAULT NULL,
   PackageImage varchar(100) DEFAULT NULL,
   Creationdate timestamp NULL DEFAULT current_timestamp(),
   UpdationDate timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
);

CREATE TABLE booking (
   BookingId integer PRIMARY KEY auto_increment,
   PackageId int(11) DEFAULT NULL,
   UserEmail varchar(100) DEFAULT NULL,
   FromDate varchar(100) DEFAULT NULL,
   ToDate varchar(100) DEFAULT NULL,
   Comment mediumtext DEFAULT NULL,
   RegDate timestamp NULL DEFAULT current_timestamp(),
   status int(11) DEFAULT NULL,
   CancelledBy varchar(5) DEFAULT NULL,
   UpdationDate timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
);
create table packageReviews (
	reviewId integer PRIMARY KEY auto_increment, 
	review VARCHAR(200),
	userId INTEGER,
	PackageId INTEGER,
	rating DECIMAL,
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
CREATE TABLE  enquiry (
   enquiryId integer PRIMARY KEY auto_increment,
   FullName varchar(100) DEFAULT NULL,
   EmailId varchar(100) DEFAULT NULL,
   MobileNumber char(10) DEFAULT NULL,
   Subject varchar(100) DEFAULT NULL,
   Description mediumtext DEFAULT NULL,
   PostingDate timestamp NULL DEFAULT current_timestamp(),
   Status int(1) DEFAULT NULL
);
CREATE TABLE issues (
   issueId integer PRIMARY KEY auto_increment,
   UserEmail varchar(100) DEFAULT NULL,
   Issue varchar(100) DEFAULT NULL,
   Description mediumtext DEFAULT NULL,
   PostingDate timestamp NULL DEFAULT current_timestamp(),
   AdminRemark mediumtext DEFAULT NULL,
   AdminremarkDate timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
);
create table category (
	categoryId integer PRIMARY KEY auto_increment, 
	title VARCHAR(50),
	description VARCHAR(100),
	createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
ALTER TABLE user ADD FOREIGN KEY (AddressId) REFERENCES address(AddressId) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE package ADD FOREIGN KEY (categoryId) REFERENCES category(categoryId) ON UPDATE CASCADE ON DELETE CASCADE;
