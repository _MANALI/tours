import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  
  firstName = ''
  lastName = ''
  phone = 0
  email = ''
  password = ''

  constructor(private service: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
   ) { 

  }

  ngOnInit(): void {
  }

  onUpdate() {
      this.service.AddData
  ( this.firstName, this.lastName, this.phone, this.email, this.password)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['home']);
          }
        })
    }


}
