import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EnqserService } from '../enqser.service';

@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: ['./issue.component.css']
})
export class IssueComponent implements OnInit {

  UserEmail = ''
  Issue = ''
  Description = ''

  constructor(private service: EnqserService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
   ) { 

  }

  ngOnInit(): void {
  }

 onIssue() {
      this.service.AddDataIssue
  ( this.UserEmail, this.Issue, this.Description)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['home']);
          }
        })
    }

}
