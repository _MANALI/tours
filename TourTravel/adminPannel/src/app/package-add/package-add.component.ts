
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';
import { CategoryService } from '../category.service';
import { PackageService } from '../package.service';
import { title } from 'process';

@Component({
  selector: 'app-package-add',
  templateUrl: './package-add.component.html',
  styleUrls: ['./package-add.component.css']
})
export class PackageAddComponent implements OnInit {

  categories = []

  PackageName = ''
  PackageType = ''
  PackageLocation = ''
  categoryId = 1
  PackagePrice = 0
  PackageFetures = ''
  PackageDetails = ''
  
  package = null

  constructor(
    private router: Router,
    private categoryService: CategoryService,
    private activatedRoute: ActivatedRoute,
    private packageService: PackageService) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.queryParams['PackageId']
    if (id) {
      // edit product
      this.packageService
        .getPackageDetails(id)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const packages = response['data']
            if (packages.length > 0) {
              this.package = this.package[0]
              this.PackageName = this.package['PackageName']
              this.PackageType = this.package['PackageType']
              this.PackageLocation = this.package['PackageLocation']
              this.categoryId = this.package['id']['categoryId']
              this.PackagePrice = this.package['PackagePrice']
              this.PackageFetures = this.package['PackageFetures']
              this.PackageDetails = this.package['PackageDetails']
            }
          }
        })
    }

  
    this.loadCategories()
  }

  loadCategories() {
    this.categoryService
      .getCategories()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.categories = response['data']
        }
      })
  }

  
  onUpdate() {

    if (this.package) {
      // edit
      this.packageService
        .updatePackage(this.package['PackageId'], this.PackageName, this.PackageType, this.PackageLocation, this.categoryId, this.PackagePrice,this.PackageFetures,this.PackageDetails)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/package-list'])
          }
        })
    } else {
      // insert
      this.packageService
        .insertPackage( this.PackageName, this.PackageType, this.PackageLocation, this.categoryId, this.PackagePrice,this.PackageFetures,this.PackageDetails)
        .subscribe(response => {
          if (response['status'] == 'success') {
            
            this.router.navigate(['/package-list'])
          }
        })
    }

  }
 
 onUploadImage() {
    this.router.navigate(['/package-upload-image'], {queryParams: {id: this.package['id']}})
  }

}