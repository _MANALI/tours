const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const fs = require('fs')

const router = express.Router()

// ----------------------------------------------------
// GET
// ----------------------------------------------------


router.get('/user', (request, response) => {
  const statement = `
      select b.PackageId, b.userId, u.firstName,u.lastName, b.PackageName, b.PackageImage, b.PackagePrice
      from bookingTable b, package p, user u 
      where b.packageId = p.PackageId AND b.userId = ${request.userId}
  `
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})
router.get('/booking', (request, response) => {
  const statement = `
      select b.PackageId, b.userId, u.firstName,u.lastName, b.PackageName, b.PackagePrice
      from bookingTable b, package p, user u 
      where b.packageId = p.PackageId AND b.userId = u.id
  `
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------


router.post('/user', (request, response) => {
  const {PackageId,PackageName,PackageType,PackageLocation,PackagePrice,PackageImage} = request.body

  const statement = `
      insert into bookingTable (PackageId,userId,PackageName,PackageType,PackageLocation,PackagePrice,PackageImage) values(
        ${PackageId},${request.userId},'${PackageName}','${PackageType}','${PackageLocation}',${PackagePrice},'${PackageImage}'
      )
  `
  console.log(statement)
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})



router.delete('/:id', (request, response) => {
  const {id} = request.params
  const statement = `delete from bookingTable where PackageId = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

/*router.put('/:id', (request, response) => {
  const {id} = request.params
  const {quantity, price} = request.body
  const totalAmount = price * quantity
  const statement = `
      update cart set quantity = ${quantity}, totalAmount = ${totalAmount}
      where id = ${id}
  `
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})*/


module.exports = router
