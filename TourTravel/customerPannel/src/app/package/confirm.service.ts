import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfirmService {

  url = 'http://localhost:4000/cart'

  constructor(
    private httpClient: HttpClient) { }
  
    getBookingPackages() {
     // add the token in the request header
     const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    
    return this.httpClient.get(this.url + "/user", httpOptions)
  }

  deleteCartItem(id) {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };
   
   return this.httpClient.delete(this.url + "/" + id, httpOptions)
 }

 updateCartItem(id, quantity, price) {
  // add the token in the request header
  const httpOptions = {
   headers: new HttpHeaders({
     token: sessionStorage['token']
   })
 };

 const body = {
   price: price, 
   quantity: quantity
 }
 
 return this.httpClient.put(this.url + "/" + id, body, httpOptions)
}

 detailsPackage(package1){
 return package1;
 }

  addCartItem(PackageId,PackageName,PackageType,PackageLocation,PackagePrice,PackageImage) {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };

   const body = {
     PackageId: PackageId,
     PackageName: PackageName,
     PackageType: PackageType,
     PackageLocation: PackageLocation,
     PackagePrice: PackagePrice,
     PackageImage: PackageImage
   }
   
   return this.httpClient.post(this.url + "/user", body, httpOptions)
 }

}
