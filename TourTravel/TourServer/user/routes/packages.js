const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const fs = require('fs')

const router = express.Router()

// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/image/:filename', (request, response) => {
  const {filename} = request.params
  const file = fs.readFileSync(__dirname + '/../../images/' + filename)
  response.send(file)
})

router.get('/details/:id', (request, response) => {
  const {id} = request.params
  const statement = `
      select p.PackageId, p.PackageName, p.PackagePrice,
        c.categoryId as category, c.title as categoryTitle,
        p.PackageDetails, p.PackageImage from package p
      inner join category c on c.categoryId = p.categoryId
      where p.id = ${id}
  `
  db.query(statement, (error, data) => {
    if (error) {
      response.send(utils.createError(error))
    } else {
      // empty products collection
      const packages = []

      // iterate over the collection and modify the structure
      for (let index = 0; index < data.length; index++) {
        const tmpPackage = data[index];
        const package1 = {
          PackageId: tmpPackage['PackageId'],
          PackageName: tmpPackage['PackageName'],
          PackgePrice: tmpPackage['PackagePrice'],
          PackageDetails: tmpPackage['PackageDetails'],
        
          category: {
            categoryId: tmpPackage['category'],
            title: tmpPackage['categoryTitle']
          },
          PackageImage: tmpPackage['PackageImage']
        }
        packages.push(package1)
      }

      response.send(utils.createSuccess(packages))
    }
  })
})

router.get('/', (request, response) => {
  const statement = `
     select p.PackageId, p.PackageName, p.PackagePrice,
        c.categoryId as category, c.title as categoryTitle,
        p.PackageDetails, p.PackageImage from package p
      inner join category c on c.categoryId = p.categoryId
  `
  db.query(statement, (error, data) => {
 if (error) {
      response.send(utils.createError(error))
    } else {
      // empty products collection
      const packages = []

      // iterate over the collection and modify the structure
      for (let index = 0; index < data.length; index++) {
        const tmpPackage = data[index];
        const package1 = {
          PackageId: tmpPackage['PackageId'],
          PackageName: tmpPackage['PackageName'],
          PackagePrice: tmpPackage['PackagePrice'],
          PackageDetails: tmpPackage['PackageDetails'],
         
          category: {
            categoryId: tmpPackage['category'],
            title: tmpPackage['categoryTitle']
          },
          PackageImage: tmpPackage['PackageImage']
        }
        packages.push(package1)
      }

      response.send(utils.createSuccess(packages))
    }
  })
})


// ----------------------------------------------------


module.exports = router
