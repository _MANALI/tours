import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutRoutingModule } from './about-routing.module';
import { AboutusComponent } from './aboutus/aboutus.component';
import { IssueComponent } from './issue/issue.component';


@NgModule({
  declarations: [AboutusComponent, IssueComponent],
  imports: [
    CommonModule,
    AboutRoutingModule
  ]
})
export class AboutModule { }
