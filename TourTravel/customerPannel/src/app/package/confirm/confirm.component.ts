import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ConfirmService } from '../confirm.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {

  items = []
  totalAmount = 0

  constructor(
    private toastr: ToastrService,
    private confirmService: ConfirmService) { }

  ngOnInit(): void {
    this.loadCartItems()
  }

  
  loadCartItems() {
    this.confirmService
      .getBookingPackages()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.items = response['data']
          this.totalAmount = 0
          for (let index = 0; index < this.items.length; index++) {
            const item = this.items[index];
            this.totalAmount += parseFloat(item['totalAmount'])
          }
        }
      })
  }
package = this.confirmService.detailsPackage

  updateQuantity(quantity, item) {
    const newQuantity = item['quantity'] + quantity
    if (newQuantity == 0) {
      this.onDelete(item)
    } else {
      this.confirmService
        .updateCartItem(item['id'], newQuantity, item['price'])
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.toastr.success('updated quantity')
            this.loadCartItems()
          }
        })
    }
  }

  onDelete(item) {
    this.confirmService
      .deleteCartItem(item['PackageId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('Your Booking Is Cancel')
          this.loadCartItems()
        }
      })
  }
 


}
