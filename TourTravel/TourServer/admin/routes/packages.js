const express = require('express'); 
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const cors= require('cors')
const multer = require('multer')
const fs = require('fs')

const upload = multer({ dest: 'images/' })

const packageRouteHandler =  express.Router(); 
 packageRouteHandler.use(cors())

var packageData = null;
packageRouteHandler.get("/", (req, res)=>{

    db.query("select * from package",(err,result)=>{
                    if(err == null  )
                    {
                        packageData = result;
                        res.contentType = "application/json";
                         console.log(result);
                        res.send(JSON.stringify(packageData));
                    }
                    else
                    {
                        console.log("Error Occured. Details are " );
                        console.log(err);
                        res.send("Something went Wrong!")
                    }
                });
});
packageRouteHandler.get("/:id", (req, res)=>{
   db.query(`select * from package where PackageId=${req.params.id}`,(err,result)=>{
    if(err == null  )
    {
        packageData = result;
        res.contentType = "application/json";
        res.send(JSON.stringify(packageData));
    }
    else
    {
        console.log("Error Occured. Details are " );
        console.log(err);
        res.send("Something went Wrong!")
    }
});
});

packageRouteHandler.post("/create", (req, res)=>{
    let packageName =  req.body.PackageName;
    let packageType =  req.body.PackageType;
    let packageLocation = req.body.PackageLocation;
   let categoryId = req.body.categoryId;
    let packageFeatures = req.body.PackageFetures;
    let packageDetails = req.body.PackageDetails;
    let price =  req.body.PackagePrice;
    let query = `insert into package (PackageName,PackageType,PackageLocation,categoryId,PackagePrice,PackageFetures,PackageDetails) values('${packageName}', '${packageType}','${packageLocation}','${categoryId}','${price}','${packageFeatures}','${packageDetails}')`;
    console.log(query);

    db.query(query,(err,result)=>{
        if(err == null  )
        {
            res.contentType = "application/json";
            res.send(JSON.stringify(result)); 
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
});
packageRouteHandler.put("/:id", (req, res)=>{
    let id = req.params.id;
    let packageName =  req.body.PackageName;
    let packageType =  req.body.PackageType;
    let packageLocation = req.body.PackageLocation;
    let price = req.body.PackagePrice;
    let packageFeature = req.body.PackageFeature;
    let packageDetails = req.body.PackageDetails;
    
    let query = `update package set PackageName='${packageName}', PackageType='${packageType}',PackageLocation='${packageLocation}',PackagePrice='${price}',PackageFetures='${packageFeature}',PackageDetails='${packageDetails}' where PackageId=${id}`;
    console.log(query);

    db.query(query,(err,result)=>{
        if(err == null  )
        {
            res.contentType = "application/json";
            res.send(JSON.stringify(result)); 
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
});

packageRouteHandler.get('/image/:filename', (request, response) => {
  const {filename} = request.params
  const file = fs.readFileSync(__dirname + '/../../images/' + filename)
  response.send(file)
})

packageRouteHandler.post('/upload-image/:PackageId', upload.single('image'), (request, response) => {
  const {PackageId} = request.params
  const fileName = request.file.filename

  const statement = `update package set PackageImage = '${fileName}' where PackageId = ${PackageId}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

packageRouteHandler.delete("/:id", (req, res)=>{
    let id = req.params.id;

    let query = `delete from package where PackageId=${id}`;
    console.log(query);

    db.query(query,(err,result)=>{
        if(err == null  )
        {
            res.contentType = "application/json";
            res.send(JSON.stringify(result)); 
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
});

module.exports = packageRouteHandler;
