import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminService } from './admin.service';
import { BookingComponent } from './booking/booking.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { PackageAddComponent } from './package-add/package-add.component';
import { PackageEditComponent } from './package-edit/package-edit.component';
import { PackageListComponent } from './package-list/package-list.component';
import { SignupComponent } from './signup/signup.component';
import { UploadImageComponent } from './upload-image/upload-image.component';
import { UserListComponent } from './user-list/user-list.component';

const routes: Routes = [{ path: 'dashboard', component: DashboardComponent , canActivate: [AdminService]},
{ path: 'user-list', component: UserListComponent, canActivate: [AdminService] },
{ path: 'package-list', component: PackageListComponent },
{ path: 'package-edit', component: PackageEditComponent },
{ path: 'booking-list', component: BookingComponent, canActivate: [AdminService] },
{ path: 'package-add', component: PackageAddComponent },
{ path: 'package-upload-image', component: UploadImageComponent, canActivate: [AdminService] },

{ path: 'login', component: LoginComponent },
{ path: 'signup', component: SignupComponent },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
