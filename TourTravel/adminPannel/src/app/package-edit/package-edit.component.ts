import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PackageService } from '../package.service';
@Component({
  selector: 'app-package-edit',
  templateUrl: './package-edit.component.html',
  styleUrls: ['./package-edit.component.css']
})
export class PackageEditComponent implements OnInit {

  PackageName = ''
  PackageType = ''
  PackageLocation = ''
  categoryId = 1
  PackagePrice = 0
  PackageFetures = ''
  PackageDetails = ''

  package;
  constructor(  private router: Router,
    private activatedRoute: ActivatedRoute,
    private packageService: PackageService) { }

  ngOnInit(): void {
  }
  getPackage() {
    const id = this.activatedRoute.snapshot.queryParams['id']
    this.packageService
      .getPackageDetails(id)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.package = response['data']
        } else {
          console.log(response['error'])
        }
      })
  }
  onEdit(){
    const id = this.activatedRoute.snapshot.queryParams['id']
    this.packageService.updatePackage(id,this.PackageName,this.PackageType,this.PackageLocation,this.categoryId,this.PackagePrice,this.PackageFetures,this.PackageDetails).subscribe(response => {
      if (response['status'] == 'success') {
        this.router.navigate['/package-list']
      } else {
        console.log(response['error'])
      }
    })
  }

}
