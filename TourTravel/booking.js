const config = require('config')
const mysql = require('mysql');     
const express = require('express'); 
const bookingRouteHandler =  express(); 

const connection = mysql.createConnection({
    host: config.get("host"),
    user: config.get("user"),
    password: config.get("password"),
    database: config.get("database")
});

var bookingData = null;
connection.connect(); 

bookingRouteHandler.get("/", (req, res)=>{

    connection.query("select * from booking",(err,result)=>{
                    if(err == null  )
                    {
                        bookingData = result;
                        res.contentType = "application/json";
                        res.send(JSON.stringify(bookingData));
                    }
                    else
                    {
                        console.log("Error Occured. Details are " );
                        console.log(err);
                        res.send("Something went Wrong!")
                    }
                });
});
bookingRouteHandler.get("/:id", (req, res)=>{
    let id = req.params.id;

    connection.query(`select * from booking where BookingId = ${id}`,(err,result)=>{
                    if(err == null  )
                    {
                        bookingData = result;
                        res.contentType = "application/json";
                        res.send(JSON.stringify(bookingData));
                    }
                    else
                    {
                        console.log("Error Occured. Details are " );
                        console.log(err);
                        res.send("Something went Wrong!")
                    }
                });
});
bookingRouteHandler.post("/", (req, res)=>{
    let id =  req.body.PackageId;
    let UserEmail =  req.body.UserEmail;
    let FromDate = req.body.FromDate;
    let ToDate = req.body.ToDate;
    let Comment = req.body.Comment;
    let status =  req.body.status;
    let CancelledBy = req.body.CancelledBy;
    let UpDationDate = req.body.UpdationDate;

    let query = `insert into booking (PackageId,UserEmail,FromDate,ToDate,Comment,status,CancelledBy,UpdationDate) values('${id}', '${UserEmail}','${FromDate}','${ToDate}','${Comment}','${status}','${CancelledBy}','${UpDationDate}')`;
    console.log(query);

    connection.query(query,(err,result)=>{
        if(err == null  )
        {
            res.contentType = "application/json";
            res.send(JSON.stringify(result)); 
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
});

bookingRouteHandler.delete("/:id", (req, res)=>{
    let id = req.params.id;

    let query = `delete from booking where BookingId=${id}`;
    console.log(query);

    connection.query(query,(err,result)=>{
        if(err == null  )
        {
            res.contentType = "application/json";
            res.send(JSON.stringify(result)); 
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
});

bookingRouteHandler.put("/:id", (req, res)=>{
    let id = req.params.id;
    let PackageId =  req.body.PackageId;
    let UserEmail =  req.body.UserEmail;
    let FromDate = req.body.FromDate;
    let ToDate = req.body.ToDate;
    let Comment = req.body.Comment;
    let status =  req.body.status;
    let CancelledBy = req.body.CancelledBy;
    let UpDationDate = req.body.UpdationDate;
    
    let query = `update booking set PackageId='${PackageId}', UserEmail='${UserEmail}',FromDate='${FromDate}',ToDate='${ToDate}',Comment='${Comment}',status='${status}',CancelledBy='${CancelledBy}',UpDationDate='${UpDationDate}' where BookingId=${id}`;
    console.log(query);

    connection.query(query,(err,result)=>{
        if(err == null  )
        {
            res.contentType = "application/json";
            res.send(JSON.stringify(result)); 
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
});
module.exports = bookingRouteHandler;