import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PackageService } from '../package.service';

@Component({
  selector: 'app-package-list',
  templateUrl: './package-list.component.html',
  styleUrls: ['./package-list.component.css']
})
export class PackageListComponent implements OnInit {

  packages;

  constructor(
    private router: Router,
    private packageService: PackageService) { }

  ngOnInit(): void {
    let notificationFromServer =  this.packageService.getPackages();
    notificationFromServer.subscribe((result)=>{
       this.packages = result;
    });
  }

  loadPackage() {
    this.packageService
      .getPackages()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.packages = response['data']
          console.log(this.packages)
        } else {
          console.log(response['error'])
        }
      })
  }

  toggleActiveStatus(product) {
    this.packageService
      .toggleActivateStatus(product)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.loadPackage()
        } else {
          console.log(response['error'])
        }
      })
  }

  onEdit(packag) {
    this.router.navigate(['/package-edit'], {queryParams: {id: packag['PackageId']}})
  }

  uploadImage(packag) {
    this.router.navigate(['/package-upload-image'], {queryParams: {id: packag['id']}})
  }

  addPackage() {
    this.router.navigate(['/package-add'])
  }
  onDelete(item) {
    this.packageService
      .deleteCartItem(item['PackageId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          
        }
      })
  }
}