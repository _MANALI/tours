import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EnqserService } from '../enqser.service';

@Component({
  selector: 'app-enquiry',
  templateUrl: './enquiry.component.html',
  styleUrls: ['./enquiry.component.css']
})
export class EnquiryComponent implements OnInit {


  FullName = ''
  EmailId = ''
  MobileNumber = 0
  Subject = ''
  Description = ''

  constructor(private service: EnqserService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
   ) { 

  }

  ngOnInit(): void {
  }

  onUpdate() {
      this.service.AddData
  ( this.FullName, this.EmailId, this.MobileNumber, this.Subject, this.Description)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['home']);
          }
        })
    }

  }


