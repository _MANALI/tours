import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CategoryService } from '../category.service';
import { PackageService } from '../package.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmComponent } from '../confirm/confirm.component';
import { ConfirmService } from '../confirm.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  packages = []
  allPackages = []
  categories = []
  constructor(
    private categoryService: CategoryService,
    private modalService: NgbModal,
    private toastr: ToastrService,
    private packageService : PackageService,
    private confirmService: ConfirmService,
    private router: Router) { }

  ngOnInit(): void {
    this.loadPacakage()
    this.loadCategories()
  }

  filterPackage(event) {
    const categoryId = event.target.value
    this.packages = []
    if (categoryId == -1) {
      this.packages = this.allPackages
    } else {
      this.packages = this.allPackages.filter(product => {
        return product.category['id'] == categoryId
      })
    }
  }

  loadCategories() {
    this.categoryService
      .getCategories()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.categories = response['data']
          this.categories.push({id: -1, title: 'All Categories'})
          console.log(this.categories)
        }
      })
  }

  /*loadCart() {
    this.modalService.open(ConfirmComponent, { size: 'lg'})
  }*/

  loadPacakage() {
    this.packageService
      .getPackages()
      .subscribe(response => {
        console.log(response)
        if (response['status'] == 'success') {
          this.allPackages = response['data']
          this.packages = this.allPackages
        }
      })
  }
onDetails(package1){
  this.confirmService.detailsPackage(package1);
  this.router.navigate(['home/package/confirm']);
}

  addToCart(package1) {
    this.confirmService
      .addCartItem(package1['PackageId'], package1['PackageName'],package1['PackageType'],package1['PackageLocation'],package1['PackagePrice'],package1['PackageImage'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('Your Booking Is Confirm')
          this.router.navigate(['home/package/confirm']);
        }
      })
  }

}
