import { Component, OnInit } from '@angular/core';
import { BookingService } from '../booking.service';
import { PackageService } from '../package.service';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {

  items = []
  totalAmount = 0

  constructor(
   
    private bookingService: BookingService) { }

  ngOnInit(): void {
    this.loadCartItems()
  }

  
  loadCartItems() {
    this.bookingService
      .getBookingPackages()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.items = response['data']
          this.totalAmount = 0
          for (let index = 0; index < this.items.length; index++) {
            const item = this.items[index];
            
          }
        }
      })
  }


  onDelete(item) {
    this.bookingService
      .deleteCartItem(item['PackageId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.loadCartItems()
        }
      })
  }
 


}
