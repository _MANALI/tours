import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MybookingRoutingModule } from './mybooking-routing.module';
import { BookingComponent } from './booking/booking.component';
import { PreviewComponent } from './preview/preview.component';


@NgModule({
  declarations: [BookingComponent, PreviewComponent],
  imports: [
    CommonModule,
    MybookingRoutingModule
  ]
})
export class MybookingModule { }
