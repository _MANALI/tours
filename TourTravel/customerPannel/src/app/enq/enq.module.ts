import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EnqRoutingModule } from './enq-routing.module';
import { EnquiryComponent } from './enquiry/enquiry.component';
import { IssueComponent } from './issue/issue.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [EnquiryComponent, IssueComponent],
  imports: [
    CommonModule,
    EnqRoutingModule,
    FormsModule
  ]
})
export class EnqModule { }
