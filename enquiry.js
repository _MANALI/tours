const mysql = require('mysql');
const express = require('express');

const app = express();


const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "manager",
    database: "Tourism"
});
app.use((req, res, next)=>{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    next();
})

app.use(express.json());
// connect to Mysql
connection.connect();

var enquiryDetails =[];


app.get("/", (req, res)=>{
    res.send(`<h1> Welcome To Enquiry Form </h1>`);
});

app.get("/enquiry", (req, res)=>{

    //Fire Query on DB
    connection.query("select * from enquiry",(err,result)=>{
                    if(err == null  )
                    {
                        enquiryDetails = result;
                        res.contentType = "application/json";
                        res.send(JSON.stringify(enquiryDetails));                    }
                    else
                    {
                        console.log("Error Occured. Details are " );
                        console.log(err);  
                        res.send("Something went Wrong!")
                    }
                });
});

app.post("/enquiry", (req, res)=>{
    
    let FullName =  req.body.FullName;
    let EmailId =  req.body.EmailId;
    let mobile =  req.body.MobileNumber;
    let subject =  req.body.Subject;
    let desc =  req.body.Description;
    let query = `insert into enquiry (FullName, EmailId, MobileNumber, Subject, Description) values ('${FullName}', '${EmailId}','${mobile}','${subject}','${desc}')`;
    console.log(query);

    connection.query(query,(err,result)=>{
        if(err == null  )
        {
            res.contentType = "application/json";
            res.send(JSON.stringify(result)); //Return the response with JSON Data
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
               //Return the response with Err Data
            res.send("Something went Wrong!")
        }
    });
});



app.listen(5000);
console.log("server starded listening");