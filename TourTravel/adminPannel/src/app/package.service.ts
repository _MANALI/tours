import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PackageService {
  url = 'http://localhost:3000/package'

  constructor(
    private httpClient: HttpClient) { }
  
  getPackages() {
     // add the token in the request header
     const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    
    return this.httpClient.get(this.url, httpOptions)
  }

  updatePackage(PackageId: number, PackageName: string, PackageType: string, PackageLocation: string, categoryId: number,PackagePrice: number,PackageFeture: string,PackageDetails:string) {
     // add the token in the request header
     const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body = {
      PackageId: PackageId,
      PackageName: PackageName,
      PackageType: PackageType,
      PackageLocation: PackageLocation,
      categoryId: categoryId,
      PackagePrice: PackagePrice,
      PackageFeture: PackageFeture,
      PackageDetails: PackageDetails,
    }
    
    return this.httpClient.put(this.url + "/" + PackageId, body, httpOptions)
  }

  insertPackage(PackageName: string, PackageType: string, PackageLocation: string, categoryId: number,PackagePrice: number,PackageFeture: string,PackageDetails:string) {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };

   const body = {
    PackageName: PackageName,
    PackageType: PackageType,
    PackageLocation: PackageLocation,
    categoryId: categoryId,
    PackagePrice: PackagePrice,
    PackageFeture: PackageFeture,
    PackageDetails: PackageDetails,
  }
  
   return this.httpClient.post(this.url + "/create", body, httpOptions)
   
 }

  getPackageDetails(id) {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };
   
   return this.httpClient.get(this.url + "/details/" + id, httpOptions)
  }

  toggleActivateStatus(packag) {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    
    const body = {}
    return this.httpClient.put(this.url + `/update-state/${packag['id']}/${packag['isActive'] == 0 ? 1 : 0}`, body, httpOptions)
  }

  uploadImage(id, file) {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body = new FormData()
    body.append('packageImage', file)

    return this.httpClient.post(this.url + `/upload-image/${id}`, body, httpOptions)
  }
  deleteCartItem(id) {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };
   
   return this.httpClient.delete(this.url + "/" + id, httpOptions)
 }
}