const config = require('config')
const mysql = require('mysql');     
const express = require('express'); 
const userRouteHandler =  express(); 

const connection = mysql.createConnection({
    host: config.get("host"),
    user: config.get("user"),
    password: config.get("password"),
    database: config.get("database")
});

var userData = null;
connection.connect(); 

userRouteHandler.get("/",(req,res) => {
    connection.query("select * from user",(err,result)=>{
        if(err == null  )
        {
            console.log(result)
            userData = result;
            res.contentType = "application/json";
            res.send(JSON.stringify(userData));
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
       });
    });
    userRouteHandler.get("/:id", (req, res)=>{
       connection.query(`select * from user where UserId=${req.params.id}`,(err,result)=>{
        if(err == null  )
        {
            userData = result;
            res.contentType = "application/json";
            res.send(JSON.stringify(userData));
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
    });
    userRouteHandler.delete("/:id",(req,res)=>{
        let no = req.params.id;

    let query = `delete from user where UserId=${no}`;
    console.log(query);

    connection.query(query,(err,result)=>{
        if(err == null  )
        {
            res.contentType = "application/json";
            res.send(JSON.stringify(result)); 
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
    });

    module.exports = userRouteHandler;