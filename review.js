const mysql = require('mysql');
const express = require('express');

const app = express();


const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "manager",
    database: "Tourism"
});
app.use((req, res, next)=>{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    next();
})

app.use(express.json());
// connect to Mysql

connection.connect();
var reviewDetails = [];

//app.get('/', (req, res) => {
//req.send("Package Reviews")
//});
app.get('/review', (req, res) => {
   
    const query = `select * from packageReviews`
    connection.query(query, (err, result) => {
        if(err == null  )
        {
            reviewDetails = result;
            res.contentType = "application/json";
            res.send(JSON.stringify(reviewDetails));                    }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);  
            res.send("Something went Wrong!")
        }
    });
  });

app.get('/:packageId', (req, res) => {
    const packageId = req.params
    const query = `select * from packageReviews where PackageId = ${packageId}`
    connection.query(query, (err, result )=> {
        if(err == null  )
        {
            reviewDetails = result;
            res.contentType = "application/json";
            res.send(JSON.stringify(reviewDetails));                    
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);  
            res.send("Something went Wrong!")
        }
    });
  });
  
  app.post("/review", (req, res)=>{
    let packagereview =  req.body.review;
    let rating =  req.body.rating;
  
    let query = `insert into packageReviews (review,rating) values('${packagereview}', '${rating}')`;
    console.log(query);

    connection.query(query,(err,result)=>{
        if(err == null  )
        {
            res.contentType = "application/json";
            res.send(JSON.stringify(result)); //Return the response with JSON Data
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
               //Return the response with Err Data
            res.send("Something went Wrong!")
        }
    });
});


  
  app.listen(5000);
  console.log("server starded listening");