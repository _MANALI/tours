const config = require('config')
const mysql = require('mysql');     
const express = require('express'); 
const packageRouteHandler =  express(); 

const connection = mysql.createConnection({
    host: config.get("host"),
    user: config.get("user"),
    password: config.get("password"),
    database: config.get("database")
});

var packageData = null;
connection.connect(); 

packageRouteHandler.get("/", (req, res)=>{

    connection.query("select * from package",(err,result)=>{
                    if(err == null  )
                    {
                        packageData = result;
                        res.contentType = "application/json";
                        res.send(JSON.stringify(packageData));
                    }
                    else
                    {
                        console.log("Error Occured. Details are " );
                        console.log(err);
                        res.send("Something went Wrong!")
                    }
                });
});
packageRouteHandler.get("/:id", (req, res)=>{
   connection.query(`select * from package where PackageId=${req.params.id}`,(err,result)=>{
    if(err == null  )
    {
        packageData = result;
        res.contentType = "application/json";
        res.send(JSON.stringify(packageData));
    }
    else
    {
        console.log("Error Occured. Details are " );
        console.log(err);
        res.send("Something went Wrong!")
    }
});
});

packageRouteHandler.post("/", (req, res)=>{
    let packageName =  req.body.PackageName;
    let packageType =  req.body.PackageType;
    let packageLocation = req.body.packageLocation;
    let packageFeatures = req.body.PackageFetures;
    let packageDetails = req.body.packageDetails;
    let price =  req.body.PackagePrice;
    let query = `insert into package (PackageName,PackageType,PackageLocation,PackagePrice,PackageFetures,PackageDetails) values('${packageName}', '${packageType}','${packageLocation}','${price}','${packageFeatures}','${packageDetails}')`;
    console.log(query);

    connection.query(query,(err,result)=>{
        if(err == null  )
        {
            res.contentType = "application/json";
            res.send(JSON.stringify(result)); 
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
});
packageRouteHandler.put("/:id", (req, res)=>{
    let id = req.params.id;
    let packageName =  req.body.PackageName;
    let packageType =  req.body.PackageType;
    let packageLocation = req.body.PackageLocation;
    let price = req.body.PackagePrice;
    let packageFeature = req.body.PackageFeature;
    let packageDetails = req.body.PackageDetails;
    
    let query = `update package set PackageName='${packageName}', PackageType='${packageType}',PackageLocation='${packageLocation}',PackagePrice='${price}',PackageFetures='${packageFeature}',PackageDetails='${packageDetails}' where PackageId=${id}`;
    console.log(query);

    connection.query(query,(err,result)=>{
        if(err == null  )
        {
            res.contentType = "application/json";
            res.send(JSON.stringify(result)); 
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
});
packageRouteHandler.delete("/:id", (req, res)=>{
    let id = req.params.id;

    let query = `delete from package where PackageId=${id}`;
    console.log(query);

    connection.query(query,(err,result)=>{
        if(err == null  )
        {
            res.contentType = "application/json";
            res.send(JSON.stringify(result)); 
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
});

module.exports = packageRouteHandler;