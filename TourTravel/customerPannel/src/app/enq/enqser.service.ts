import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnqserService {

  constructor(public http: HttpClient) { }

  AddData(FullName: string,EmailId:string,MobileNumber:number,Subject:string,Description:string){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = {
      FullName: FullName,
      EmailId: EmailId,
      MobileNumber: MobileNumber,
      Subject: Subject,
      Description: Description
    }
    
    return this.http.post("http://localhost:4000/enquiry",body,httpOptions );
  }

  AddDataIssue(UserEmail: string,Issue:string,Description:string){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    const body = {
      UserEmail: UserEmail,
      Issue: Issue,
      Description: Description
    }
    
    return this.http.post("http://localhost:4000/enquiry/issue",body,httpOptions );
  }
}
