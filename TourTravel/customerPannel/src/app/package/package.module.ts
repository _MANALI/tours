import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PackageRoutingModule } from './package-routing.module';
import { GalleryComponent } from './gallery/gallery.component';
import { ConfirmComponent } from './confirm/confirm.component';


@NgModule({
  declarations: [GalleryComponent, ConfirmComponent],
  imports: [
    CommonModule,
    PackageRoutingModule
  ]
})
export class PackageModule { }
