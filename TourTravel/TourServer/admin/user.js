const express = require('express'); 
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const cors= require('cors')

const userRouteHandler =  express.Router(); 
 userRouteHandler.use(cors())

var userData = null;


userRouteHandler.get("/",(req,res) => {
    db.query("select * from user",(err,result)=>{
        if(err == null  )
        {
            console.log(result)
            userData = result;
            res.contentType = "application/json";
            res.send(JSON.stringify(userData));
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
       });
    });
    userRouteHandler.get("/:id", (req, res)=>{
       db.query(`select * from user where UserId=${req.params.id}`,(err,result)=>{
        if(err == null  )
        {
            userData = result;
            res.contentType = "application/json";
            res.send(JSON.stringify(userData));
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
    });
    userRouteHandler.delete("/:id",(req,res)=>{
        let no = req.params.id;

    let query = `delete from user where UserId=${no}`;
    console.log(query);

    db.query(query,(err,result)=>{
        if(err == null  )
        {
            res.contentType = "application/json";
            res.send(JSON.stringify(result)); 
        }
        else
        {
            console.log("Error Occured. Details are " );
            console.log(err);
            res.send("Something went Wrong!")
        }
    });
    });

    module.exports = userRouteHandler;
