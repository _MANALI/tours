const express = require('express'); 
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const uuid = require('uuid')
const cors= require('cors')

const fs = require('fs')

const enquiryRouteHandler =  express.Router(); 


enquiryRouteHandler.post("/", (request, response)=>{
   const {FullName, EmailId,MobileNumber,Subject,Description} = request.body
    const statement = `insert into enquiry (FullName,EmailId,MobileNumber,Subject,Description) values ('${FullName}', '${EmailId}',${MobileNumber},'${Subject}','${Description}')`


   console.log(statement)
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
 
})

enquiryRouteHandler.post("/issue", (request, response)=>{
   const {UserEmail, Issue,Description} = request.body
    const statement = `insert into issues (UserEmail,Issue,Description) values ('${UserEmail}', '${Issue}','${Description}')`


   console.log(statement)
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
 
})


module.exports = enquiryRouteHandler;
